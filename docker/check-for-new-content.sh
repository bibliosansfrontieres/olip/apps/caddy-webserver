#!/bin/sh
(
  flock -n -x 200 || exit 1

  number_of_current_content=`ls -1 /data/content/ | wc -l`

  if [ ! -f "/data/number_of_content.txt" ];
  then
    number_of_previous_content="0"
  else
    number_of_previous_content=`cat /data/number_of_content.txt`
  fi

  if [ "$number_of_current_content" -gt "$number_of_previous_content" -a "$number_of_current_content" -ne "0" -a "$number_of_current_content" -le "1" ];
  then
    echo "$number_of_current_content" > /data/number_of_content.txt

    echo "[+]  Look for first index.html file in /data/content/ tree"
    website_dir=`find /data/content/ -type f -name index.html -exec dirname '{}' ';' | sort | head -1`

    echo "[+]  Update Caddy with new root website"
    curl localhost:2019/config/apps/http/servers/srv0/routes/0/handle/0/root -X POST -H "Content-Type: application/json" -d '"'$website_dir'"'
  elif [ "$number_of_current_content" -le "0" -o  "$number_of_current_content" -gt "1" ]
  then
    echo 0 > /data/number_of_content.txt

    echo "No website or more than one website folder has been found in /data/content"
    curl localhost:2019/config/apps/http/servers/srv0/routes/0/handle/0/root -X POST -H "Content-Type: application/json" -d '"/data/"'
  else
    echo "Nothing to do"
  fi
) 200>/var/lock/.hugo.exclusivelock
