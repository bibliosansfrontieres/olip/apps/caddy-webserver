#!/bin/sh

# Copy welcome page
cp -f /docker/index.html /data/index.html

# Clean up number of content on reboot
rm -f /data/number_of_content.txt

# Start Crontab
/usr/sbin/crond -f -l 8 &

exec "$@"
